import { IsNotEmpty, IsPositive, MinLength } from 'class-validator';

export class CreateProductsDto {
  @IsNotEmpty()
  @MinLength(8)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  price: number;
}
